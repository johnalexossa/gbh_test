# gbh_test



## Parte 1:

Considering the following (Node.js) applications and their repositories:

Demo API (https://github.com/gbh-tech/demo-api).
Demo WebApp Client (https://github.com/gbh-tech/demo-webapp).

Create an Ansible playbook that can deploy and configure these sample applications from scratch. Ideally you should create a VM, either locally or remote (on AWS, Azure, GCP, DigitalOcean, etc) to test it out. The acceptance criteria here is that after running your playbook, both applications should be up and running, and the WebApp client is communicating successfully with the API.

Se debe tener 2 al menos 2 instancias ec2, en las cuales se va a desplegar por medio del playbook las 2 aplicaciones, el playbook se debe ejecutar desde una instancia que tengan ansible configurado y en cuyo inventario tenga configurado las 2 instancias ec2. el playbook se llama deploy_app.yaml y un ejemplo para correrlo es:


```
ansible-playbook --extra-vars "host=nodo1 dir=/app repo=https://github.com/gbh-tech/demo-api.git" deploy_app.yaml -b
```

-  host: Nombre del host dentro del inventario Ansible
-  dir: Directorio donde se debe desplegar el codigo de la aplicacion
-  repo: Repositorio desde donde se va a obtener el codigo de la aplicacion 

## Parte 3:

Se crea una shell (parte3.sh), que primero valida si estan instalados git y docker, de no ser asi, los instala. 
Luego descarga las aplicaciones demo-api y demo-webapp desde los repositorios y genera las imagenes con docker.
Al final por medio de dockercompse, se generan los contenedores

## Parte 4:

Se crea una configuracion de terraform para desplegar un cluster de eks con dos nodos.
crea su propia vpc y segurity groups