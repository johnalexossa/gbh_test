#!/bin/bash

### VALIDAR E INSTALAR DOCKER
echo '... Validando Docker ....'
if [[ $(which docker) && $(docker --version) ]]; then
    echo " ... Docker ya se encuentra INSTALADO !!!!"
    
  else
    echo '.... Docker no encontrado... Instalando docker ...'
    sudo yum update -y && sudo yum install docker -y
    newgrp docker
    sudo usermod -a -G docker ec2-user
    sudo chkconfig docker on
    sudo service docker start
    echo '... Docker Instalado !!!!!'
fi
echo '---------------------'
### VALIDAR E INSTALAR GIT
echo '... Validando Git ....'
if [[ $(which git) && $(git --version) ]]; then
    echo " ... GIT ya se encuentra INSTALADO !!!!"
    
  else
    echo '.... Git no encontrado... Instalando Git ...'
    sudo yum update -y && sudo yum install git -y
    echo '... Docker Instalado !!!!!'
fi

echo '---------------------'
### PREPARANDO AMBIENTE
echo '... Preparando ambiente ....'
sudo mkdir /ambiente
sudo chmod +x /ambiente
sudo chown ec2-user /ambiente
cd /ambiente

echo '---------------------'
### CREAR IMAGEN demo-api
echo '...  Creando Imagen demo-api ....'
git clone https://github.com/gbh-tech/demo-api.git
cd demo-api
touch Dockerfile
echo 'FROM node:16

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 3001
CMD [ "npm", "start" ]" ' > Dockerfile

docker build . -t demo-api

echo '... Imagen demo-api generada !!!!!'
echo '---------------------'
cd /ambiente

### CREAR IMAGEN  demo-webapp
echo '...  Creando Imagen demo-webapp ....'
git clone https://github.com/gbh-tech/demo-webapp.git
cd demo-webapp
touch Dockerfile
echo 'FROM node:16

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 3002
CMD [ "npm", "start" ]" ' > Dockerfile

docker build . -t demo-webapp
echo '... Imagen demo-webapp generada !!!!!'

echo '---------------------'
cd /ambiente
mkdir my-db

### CREAR DOCKERCOMPOSE FILE
echo '...  Creando archivo DockerCompose ....'
touch docker-compose.yml
echo "version: '3'
services:
    demoapi:
        container_name: demo_api
        image: demo-api
        ports:
            - '3001:3001'
        networks:
            - net
    webapp:
        container_name: demo_webapp_1
        image: demo-webapp
        ports:
            - '3002:3002'
        networks:
            - net
    webapp2:
        container_name: demo_webapp_2
        image: demo-webapp
        ports:
            - '3003:3002'
        networks:
            - net

    db_host:
        container_name: db
        image: mysql:5.7
        environment:
            - 'MYSQL_ROOT_PASSWORD=1234'
        volumes:
            - $PWD/my-db:/var/lib/mysql
        networks:
            - net
networks:
  net: " > docker-compose.yml

echo '---------------------'

### INICIAR CONTENEDORES
echo '...  Iniciando contenedores  ....'
docker-compose up -d
























